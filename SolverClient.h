// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __SOLVERCLIENT_H__
#define __SOLVERCLIENT_H__

#include <string>
#include <atomic>
#include <memory>
#include <array>
#include <mutex>
#include <thread>

extern "C" {
  #include "jansson.h"
}

#include "SynchronizedQueue.h"
#include "ClientSocket.h"
#include "Communicator.h"
#include "CrushState.h"
#include "crush-types.h"
#include "BoardCopies.h"

using namespace std;

enum class SolverClientConnectionStatus {
  Inactive,
  Handshaking,
  Active
};

/* Given to the solver's priority queue to order the most desirable board. */
class BoardCopiesComparer {
  public:
    bool operator() (
        const std::shared_ptr<BoardCopies>& x,
        const std::shared_ptr<BoardCopies>& y) {

      if (x->depth != y->depth) {
        return x->depth > y->depth;
      }

      return x->score < y->score;
    }
};

class SolverClient {
public:
   SolverClient() :
     connectionStatus(SolverClientConnectionStatus::Inactive),
     movesEvaluated(0), running(false), bestCopy(NULL) { }
   ~SolverClient();
   void Start(std::string host, int port);
   void addBoard(std::shared_ptr<BoardCopies> value);
   void exploreMoves(std::shared_ptr<BoardCopies> board);

   static const int kNumThreads = 8;

private:
   void workFunc();

   json_t* stringToJson(string toJson);
   const char* actionToString(json_t * act);

   void sendBestMove();
   bool modelRead();

   hw5_net::ClientSocket* clientSocket;
   SolverClientConnectionStatus connectionStatus;

   std::atomic_uint movesEvaluated;
   std::atomic_bool running;

   SynchronizedQueue<
     std::shared_ptr<BoardCopies>, BoardCopiesComparer
     >* boardQueue;

   shared_ptr<BoardCopies> bestCopy;
   std::mutex bestCopyMutex;
   std::array<std::unique_ptr<std::thread>, kNumThreads>* workers;
};

#endif
