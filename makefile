CC=gcc
CXX=g++

SRC=CrushManager.cc CrushState.cc ServerCommunicatorWrap.cc LocalCommunicator.cc \
		SocketServerCommunicator.cc SocketClientCommunicator.cc BoardCopies.cc

HEADERS=crush-grid.h crush-app.h crush-app-window.h CrushManager.h crush-types.h \
		CrushState.h ServerCommunicatorWrap.h \
		Communicator.h LocalCommunicator.h \
		SocketServerCommunicator.h SolverClient.h \
		BoardCopies.h SynchronizedQueue.h

CRUSHSRC=$(SRC) crush.cc
VIEWSRC=$(SRC) NetworkedView.cc
MODELSRC=$(SRC) NetworkedModel.cc
SOLVERSRC=$(SRC) NetworkedSolver.cc SolverClient.cc

VIEWOBJS=crush-app.o crush-grid.o crush-app-window.o resource.o

LIBS=../array2d/libarray2d.a serverLibrary/libserver.a # include array2d library, serverLib.
CFLAGS=-Wall -Wpedantic -std=c11 # -g3
CPPFLAGS=-O3 -Wall -Wpedantic -std=c++11 # -g3
IFLAGS=-I../array2d -I../jansson/include -IserverLibrary

GTKCFLAGS = `pkg-config --cflags gtk+-3.0`
LDFLAGS =  -L../array2d -L../jansson/lib -LserverLibrary -larray2d -ljansson -lserver -lpthread
GTKLDFLAGS = `pkg-config --libs gtk+-3.0`
GLIB_COMPILE_RESOURCES = glib-compile-resources

all: hw6-model

crush: $(CRUSHSRC) $(HEADERS) $(LIBS) $(VIEWOBJS)
	$(CXX) $(CPPFLAGS) $(IFLAGS) $(GTKCFLAGS) -o $@ $(VIEWOBJS) $(CRUSHSRC) $(LDFLAGS) $(GTKLDFLAGS)

hw5-view: $(VIEWSRC) $(HEADERS) $(LIBS) $(VIEWOBJS)
	$(CXX) $(CPPFLAGS) $(IFLAGS) $(GTKCFLAGS) -o $@ $(VIEWOBJS) $(VIEWSRC) $(LDFLAGS) $(GTKLDFLAGS)

hw5-model: $(MODELSRC) $(MODEL) $(HEADERS) $(LIBS)
	$(CXX) $(CPPFLAGS) $(IFLAGS) -o $@ $(MODELSRC) $(MODEL) $(LDFLAGS)

hw6-model: $(SOLVERSRC) $(MODEL) $(HEADERS) $(LIBS)
	$(CXX) $(CPPFLAGS) $(IFLAGS) -o $@ $(SOLVERSRC) $(MODEL) $(LDFLAGS)

# Tell make how to build libarray2d if needed.
../array2d/libarray2d.a:
	$(MAKE) -C ../array2d

./serverLibrary/libserver.a:
	$(MAKE) -C ./serverLibrary

# Generate resource.c from resource XML.
resource.c: crush.gresource.xml $(shell $(GLIB_COMPILE_RESOURCES) --generate-dependencies crush.gresource.xml)
	$(GLIB_COMPILE_RESOURCES) --target=resource.c --generate-source crush.gresource.xml

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) $(IFLAGS) $(GTKCFLAGS) -c $<

clean:
	rm -f *.o *~ resource.c hw6-model
