// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __LOCALCOMMUNICATOR_H__
#define __LOCALCOMMUNICATOR_H__

#include "CrushManager.h"
#include "crush-types.h"

class LocalCommunicator : public ClientCommunicator, public ServerCommunicator {
public:
   LocalCommunicator() : updateUiCallback(NULL), crushManager(NULL) { }

   virtual void Update(CrushState* state);
   virtual void Move(int8_t, int8_t, int);

   void SetCrushManager(CrushManager* mgr) {
      this->crushManager = mgr;
   }

   void SetCallback(UpdateUiCallback callback) {
      this->updateUiCallback = callback;
   }

   virtual CrushState* GetState() const {
      return this->state;
   }

   virtual void SetState(CrushState* state) {
      this->state = state;
   }

private:
   UpdateUiCallback updateUiCallback;
   CrushManager* crushManager;
   CrushState* state;
};

#endif
