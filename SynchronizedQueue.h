// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __SYNCHRONIZEDQUEUE_H__
#define __SYNCHRONIZEDQUEUE_H__

#include <queue>
#include <mutex>
#include <condition_variable>
#include <iostream>

/**
 * A producer-consumer queue supporting multiple concurrent readers.
 */
template <typename T, typename Cmp>
class SynchronizedQueue
{
    std::priority_queue<T, std::vector<T>, Cmp> queue_;
    std::mutex mutex_;
    std::condition_variable condvar_;

    typedef std::lock_guard<std::mutex> lock;
    typedef std::unique_lock<std::mutex> ulock;

public:
    SynchronizedQueue() : queue_(Cmp()) { }

    void put(T const &val)
    {
        lock l(mutex_); // prevents multiple pushes corrupting queue_

        bool wake = queue_.empty(); // we may need to wake consumer
        queue_.push(val);
        if (wake) {
          condvar_.notify_one();
        }
    }

    T get()
    {
        ulock u(mutex_);
        while (queue_.empty()) {
            condvar_.wait(u);
        }
        // now queue_ is non-empty and we still have the lock
        T retval = queue_.top();
        queue_.pop();
        return retval;
    }

    /* Only for debugging -- destroys the front of the queue. */
    void printFront()
    {
        ulock u(mutex_);

        std::cerr << " queue size " << this->queue_.size() << std::endl;

        int i = 0;
        while (!queue_.empty() && i < 40) {
          std::cerr << " Queue item " << *(queue_.top()) << std::endl;
          queue_.pop();
          i++;
        }
        u.unlock();
    }

  void dump(){
    ulock lock(mutex_);
    std::priority_queue<T, std::vector<T>, Cmp> empty;
    swap(queue_, empty);
    lock.unlock();
  }
};

#endif // __SYNCHRONIZEDQUEUE_H__
