// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __COMMUNICATOR_H__
#define __COMMUNICATOR_H__

#include "CrushState.h"

class ClientCommunicator {
public:
   virtual ~ClientCommunicator() { }
   virtual void Update(CrushState* state) = 0;
};

class ServerCommunicator {
public:
   virtual ~ServerCommunicator() { }
   virtual void Move(int8_t, int8_t, int) = 0;
   virtual CrushState* GetState() const = 0;
   virtual void SetState(CrushState*) = 0;
   virtual void Start() {
      // Optional.
   }
   virtual void SendBye(){}
};

#endif
