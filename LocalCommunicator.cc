// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <cassert>

#include "LocalCommunicator.h"

void LocalCommunicator::Update(CrushState* state) {
   assert(this->updateUiCallback != NULL);

   this->state = state;

   this->updateUiCallback();
}

void LocalCommunicator::Move(int8_t sourceRow, int8_t sourceCol, int direction) {
   assert(this->crushManager != NULL);

   this->crushManager->swap(sourceRow, sourceCol, (SwapDirection)direction);
}
