// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

extern "C" {
  #include <gtk/gtk.h>

  #include "crush-app.h"

  void* gServerCommunicator = NULL;
  int gUseNetwork = 0;
}

int main(int argc, char** argv) {
  CrushApp* app = crush_app_new();
  int result = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);
  return result;
}
