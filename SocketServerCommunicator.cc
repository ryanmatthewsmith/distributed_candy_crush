// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <iostream>
#include <string>
#include <cstring>
#include <cassert>

extern "C" {
  #include "jansson.h"
}

#include "ServerSocket.h"
#include "ClientSocket.h"

#include "SocketServerCommunicator.h"

using namespace std;

void SocketServerCommunicator::Listen() {
  this->connectionState = ConnectionState::Listening;
  int ss_fd;
  this->serverSocket = new hw5_net::ServerSocket(0);
  this->serverSocket->BindAndListen(AF_INET, &ss_fd);
  cout << "Created bound socket. port = " << this->serverSocket->port() << endl;
}

void SocketServerCommunicator::AcceptClient() {
  //all Accept Reqs
  int accepted_fd;
  string clientAddr;
  uint16_t clientPort;
  string clientDNSName;
  string serverAddress;
  string serverDNSName;

  assert(this->serverSocket != NULL);

  this->serverSocket->Accept( &accepted_fd, &clientAddr, &clientPort,
     &clientDNSName, &serverAddress, &serverDNSName );

  this->connectionState = ConnectionState::Handshaking;

  // wrap connection to peer with a CientSocket
  this->clientSocket = new hw5_net::ClientSocket(accepted_fd);
  this->viewRead();
  // read the update:
  this->viewRead();

  this->connectionState = ConnectionState::Active;
}

void SocketServerCommunicator::Move(int8_t sourceRow, int8_t sourceCol, int direction) {
  json_t* json = json_object();
  json_object_set_new(json, "action", json_string("move"));
  json_object_set_new(json, "row", json_integer(sourceRow));
  json_object_set_new(json, "column", json_integer(sourceCol));
  json_object_set_new(json, "direction", json_integer(direction));

  char* payload = json_dumps(json, 0);
  json_decref(json);

  this->clientSocket->WrappedWrite(payload, strlen(payload));
  free(payload);
  viewRead();
}

void SocketServerCommunicator::SendBye(){
  this->connectionState = ConnectionState::Listening;

  json_t* json = json_object();
  json_object_set_new(json, "action", json_string("bye"));

  char* payload = json_dumps(json, 0);
  json_decref(json);

  this->clientSocket->WrappedWrite(payload, strlen(payload));
  free(payload);
}

void SocketServerCommunicator::helloFunc() {
  json_t* json = json_object();
  json_object_set_new(json, "action", json_string("helloack"));
  assert(this->state != NULL);
  json_object_set_new(json, "gameinstance", this->state->toJson());

  char* payload = json_dumps(json, 0);
  json_decref(json);
  this->clientSocket->WrappedWrite(payload, strlen(payload));
  free(payload);
}

void SocketServerCommunicator::updateFunc() {
  //redraw Update
  if (this->connectionState == ConnectionState::Active) {
    assert(this->updateUiCallback != NULL);
    this->updateUiCallback();
  }
}

json_t* SocketServerCommunicator::stringToJson(string toJson){
  const char *cstr = toJson.c_str();
  return json_loads(cstr, JSON_DECODE_ANY, NULL);
}

const char * SocketServerCommunicator::actionToString(json_t * act){
  json_t * actionStr = json_object_get(act, "action");
  if (actionStr == NULL)printf("uh oh, no action in json");
  const char *actionCstr = json_string_value(actionStr);
  return actionCstr;
}

void SocketServerCommunicator::viewRead() {
  if(!(this->clientSocket->isConnected())) {
    this->connectionState = ConnectionState::Listening;
    return;
  }

  char buffer[1024];
  int bracesCount = 0;
  string bufInString;
  this->clientSocket->WrappedRead(buffer, 1);
  if (buffer[0] == '{'){
    bufInString+=buffer[0];
    bracesCount++;
  } else{
    cout << "not a json on read" << endl;
  }
  uint16_t i = 0;
  while(bracesCount != 0){
    this->clientSocket->WrappedRead(buffer, 1023);
    for (; i< 1023 ; i++){
      bufInString += buffer[i];
      if (buffer[i] == '{') bracesCount++;
      if (buffer[i] == '}') bracesCount--;
      if (bracesCount == 0)break;
      }
      i=0;
  }

  json_t *json = stringToJson(bufInString);
  const char* action = actionToString(json);

  if (strcmp(action, "hello") == 0) {
    if (this->connectionState == ConnectionState::Handshaking) {
      helloFunc();
    } else {
      cerr << "action \"hello\" invalid for "
        "current connection state." << endl;
    }
  }
  else if (strcmp(action, "update") == 0) {
    if (this->connectionState == ConnectionState::Active
     || this->connectionState == ConnectionState::Handshaking) {

      this->SetState(new CrushState(
       json_object_get(json, "gameinstance")));

      updateFunc();
    } else {
      cerr << "action \"update\" invalid for "
        "current connection state." << endl;
    }
  } else {
    cerr << "Invalid message action \"" << action << "\"" << endl;
  }

  json_decref(json);
}
