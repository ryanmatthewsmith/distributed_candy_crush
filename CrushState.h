// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __CRUSHSTATE_H__
#define __CRUSHSTATE_H__

#include <vector>

extern "C" {
   #include <jansson.h>
   #include "array2D.h"
}

#include "crush-types.h"

/**
 * CrushState.
 * Handles serializing/deserializing game state.
 */
class CrushState {
public:
   /**
    * Constructs a CrushState with the given JSON.
    */
   CrushState(json_t* jsonInput);

   /*
    * Copy constructor. (Deep copy.)
    */
   CrushState(const CrushState&);

   ~CrushState();

   int getMoveCount() const { return this->moveCount; }
   int getScore() const { return this->score; }
   Array2D getBoardArray() const { return this->boardArray; }
   Array2D getBoardState() const { return this->boardState; }

   bool isNewGame() const { return this->newGame; }

   /**
    * Returns a pointer to a json payload, fully representing the current game
    * state. Returned value is reference counted and must be cleaned up with
    * json_decref().
    */
   json_t* toJson() const;

   int moveCount;
   int score;
   bool newGame;
   json_t* gameDefJson;
   Array2D boardArray;
   Array2D boardState;
   Array2D extBoard;
   std::vector<int> extensionOffsets;
   int rows;
   int cols;
};

#endif
