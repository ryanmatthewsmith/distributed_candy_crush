// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

/* crush-app.h */

#ifndef __CRUSH_APP_H__
#define __CRUSH_APP_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CRUSH_TYPE_APP crush_app_get_type()

#define CRUSH_APP(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  CRUSH_TYPE_APP, CrushApp))

#define CRUSH_APP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  CRUSH_TYPE_APP, CrushAppClass))

#define CRUSH_IS_APP(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  CRUSH_TYPE_APP))

#define CRUSH_IS_APP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  CRUSH_TYPE_APP))

#define CRUSH_APP_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  CRUSH_TYPE_APP, CrushAppClass))

typedef struct _CrushApp CrushApp;
typedef struct _CrushAppClass CrushAppClass;
typedef struct _CrushAppPrivate CrushAppPrivate;

struct _CrushApp
{
  GtkApplication parent;

  CrushAppPrivate* priv;
};

struct _CrushAppClass
{
  GtkApplicationClass parent_class;
};

GType crush_app_get_type (void) G_GNUC_CONST;

CrushApp *crush_app_new (void);

G_END_DECLS

#endif /* __CRUSH_APP_H__ */
