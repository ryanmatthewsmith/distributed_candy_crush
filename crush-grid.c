// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

/* crush-grid.c */

#include <assert.h>
#include <stdlib.h>

#include "array2D.h"

#include "crush-grid.h"
#include "crush-types.h"

#define GRID_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CRUSH_TYPE_GRID, CrushGridPrivate))

extern Array2D gBoardArray;
extern Array2D gBoardState;

// Holds bookkeeping info for each grid position, passed in candy selection
// callback.
typedef struct boardData_st {
   int row;
   int col;
   GtkWidget* button;
   GtkWidget* image;
} BoardData;

typedef enum {
  JellyStateClear = 0,
  JellyStatePartial = 1,
  JellyStateFull = 2,
  JellyStateCount = 3
} JellyState;

struct _CrushGridPrivate {
  GdkPixbuf* colorPixbufs[ColorCount][JellyStateCount];
  GdkPixbuf* emptyPixbuf;
  int currRow;
  int currCol;
  int boardColumns;
  BoardData* boardArray;
};

G_DEFINE_TYPE_WITH_PRIVATE (CrushGrid, crush_grid, GTK_TYPE_GRID)

// Retrieve a ptr to the boardArray instance at row x, col y.
#define GET_APP_DATA_PTR(priv, x, y) \
  ((priv)->boardArray + ((x) * (priv)->boardColumns + (y)))


static void
crush_grid_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
crush_grid_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
crush_grid_dispose (GObject *object)
{
  G_OBJECT_CLASS (crush_grid_parent_class)->dispose (object);
}

static void
crush_grid_finalize (GObject *object)
{
  CrushGrid* grid = CRUSH_GRID(object);
  CrushGridPrivate* priv = crush_grid_get_instance_private(grid);

  // Free the pixbufs.
  g_object_unref(priv->emptyPixbuf);

  for (int i = 0; i < ColorCount; i++) {
    for (int j = 0; j < JellyStateCount; j++) {
      g_object_unref(priv->colorPixbufs[i][j]);
    }
  }

  free(priv->boardArray);

  G_OBJECT_CLASS (crush_grid_parent_class)->finalize (object);
}

static void
crush_grid_class_init (CrushGridClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = crush_grid_get_property;
  object_class->set_property = crush_grid_set_property;
  object_class->dispose = crush_grid_dispose;
  object_class->finalize = crush_grid_finalize;
}

static void selectButton(GtkWidget *button, gpointer dataIn) {
   BoardData *data = dataIn;

   CrushGrid* grid = CRUSH_GRID(
    gtk_widget_get_ancestor(button, CRUSH_TYPE_GRID));

   crush_grid_deselect(grid);

   CrushGridPrivate* priv = crush_grid_get_instance_private(grid);

   priv->currCol = data->col;
   priv->currRow = data->row;
   gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NORMAL);
}

/*
 * Each of our six colors has three jelly state images. Load all of them, and
 * the empty image used for testing.
 */
static void loadColorPixbufs(CrushGrid* grid) {
  CrushGridPrivate* priv = crush_grid_get_instance_private(grid);
  GError* error;

  priv->emptyPixbuf = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/empty.png", &error);

  priv->colorPixbufs[Blue][JellyStateClear] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/blue_0.png", &error);
  priv->colorPixbufs[Green][JellyStateClear] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/green_0.png", &error);
  priv->colorPixbufs[Orange][JellyStateClear] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/orange_0.png", &error);
  priv->colorPixbufs[Purple][JellyStateClear] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/purple_0.png", &error);
  priv->colorPixbufs[Red][JellyStateClear] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/red_0.png", &error);
  priv->colorPixbufs[Yellow][JellyStateClear] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/yellow_0.png", &error);

  priv->colorPixbufs[Blue][JellyStatePartial] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/blue_1.png", &error);
  priv->colorPixbufs[Green][JellyStatePartial] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/green_1.png", &error);
  priv->colorPixbufs[Orange][JellyStatePartial] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/orange_1.png", &error);
  priv->colorPixbufs[Purple][JellyStatePartial] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/purple_1.png", &error);
  priv->colorPixbufs[Red][JellyStatePartial] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/red_1.png", &error);
  priv->colorPixbufs[Yellow][JellyStatePartial] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/yellow_1.png", &error);

  priv->colorPixbufs[Blue][JellyStateFull] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/blue_2.png", &error);
  priv->colorPixbufs[Green][JellyStateFull] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/green_2.png", &error);
  priv->colorPixbufs[Orange][JellyStateFull] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/orange_2.png", &error);
  priv->colorPixbufs[Purple][JellyStateFull] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/purple_2.png", &error);
  priv->colorPixbufs[Red][JellyStateFull] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/red_2.png", &error);
  priv->colorPixbufs[Yellow][JellyStateFull] = gdk_pixbuf_new_from_resource(
   "/edu/uw/crush/images/40x40/yellow_2.png", &error);
}

/* Returns the explosions remaining for the given square. */
static inline CandyInt getJellyExplosionsLeft(int8_t row, int8_t col) {
  ArrValue value;
  Array2D_get(gBoardState, row, col, &value);
  return *(CandyInt*) value;
}

static inline GdkPixbuf* getPixbuf(const CrushGridPrivate* priv,
 CandyColor color, int remaining) {

  if (Empty == color) {
    return priv->emptyPixbuf;
  }

  assert(color >= 0 && color < ColorCount);

  int jellyStateIndex =
   remaining > JellyStateFull ? JellyStateFull : remaining;

  return priv->colorPixbufs[color][jellyStateIndex];
}

static void crush_grid_init(CrushGrid *self)
{
  self->priv = GRID_PRIVATE(self);

  loadColorPixbufs(self);

  int r = Array2D_getRows(gBoardArray);
  int c = Array2D_getCols(gBoardArray);

  self->priv->boardColumns = c;
  self->priv->boardArray = (BoardData*) malloc(sizeof(BoardData) * r * c);

  if (self->priv->boardArray == NULL) {
    fprintf(stderr, "Unable to allocate space for grid data.\n");
    exit(1);
  }

  int rowFlip = r - 1;

  for (int i = 0; i < r; i++) {
    for (int j = 0; j < c; j++) {
      ArrValue val;
      Array2D_get(gBoardArray, i, j, &val);
      BoardCandy* boardCandy = (BoardCandy*) val;

      CandyInt explosionsRemaining = getJellyExplosionsLeft(i, j);

      GtkWidget* image = gtk_image_new_from_pixbuf(
       getPixbuf(self->priv, boardCandy->color, explosionsRemaining));

      BoardData* appDataItem = GET_APP_DATA_PTR(self->priv, i, j);

      appDataItem->row = i;
      appDataItem->col = j;
      appDataItem->button = gtk_button_new();
      appDataItem->image = image;

      gtk_button_set_image(GTK_BUTTON(appDataItem->button), image);

      g_signal_connect(appDataItem->button, "clicked",
                       G_CALLBACK(selectButton), appDataItem);

      gtk_button_set_relief(GTK_BUTTON(appDataItem->button), GTK_RELIEF_NONE);
      gtk_grid_attach(GTK_GRID(self), appDataItem->button, j, rowFlip, 1, 1);
    }//for
    rowFlip--;
  }//for
  gtk_widget_show_all(GTK_WIDGET(self));
}

CrushGrid* crush_grid_new(CrushApp *app)
{
  return g_object_new(CRUSH_TYPE_GRID, NULL);
}

void crush_grid_update_buttons(CrushGrid* grid) {
  CrushGridPrivate* priv = crush_grid_get_instance_private(grid);

  int r = Array2D_getRows(gBoardArray);
  int c = Array2D_getCols(gBoardArray);

  for (int i = 0; i < r; i++) {
    for (int j = 0; j < c; j++) {
      ArrValue val;
      Array2D_get(gBoardArray, i, j, &val);
      BoardCandy* boardCandy = (BoardCandy*) val;

      // Change the pixbuf that the button image uses.
      BoardData* dataPtr = GET_APP_DATA_PTR(priv, i, j);

      CandyInt explosionsRemaining = getJellyExplosionsLeft(i, j);

      gtk_image_set_from_pixbuf(
       GTK_IMAGE(dataPtr->image),
       getPixbuf(priv, boardCandy->color, explosionsRemaining));
   }
 }
}

void crush_grid_deselect(CrushGrid* grid) {
  CrushGridPrivate* priv = crush_grid_get_instance_private(grid);

   if (priv->currRow != -1 && priv->currCol != -1) {
     BoardData* prev = GET_APP_DATA_PTR(priv, priv->currRow, priv->currCol);
     gtk_button_set_relief(GTK_BUTTON(prev->button), GTK_RELIEF_NONE);
     priv->currRow = -1;
     priv->currCol = -1;
   }
}

int8_t crush_grid_selected_row(CrushGrid* grid) {
  CrushGridPrivate* priv = crush_grid_get_instance_private(grid);
  return priv->currRow;
}

int8_t crush_grid_selected_column(CrushGrid* grid) {
  CrushGridPrivate* priv = crush_grid_get_instance_private(grid);
  return priv->currCol;
}
