// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

/* crush-grid.h */

#ifndef __CRUSH_GRID_H__
#define __CRUSH_GRID_H__

#include <gtk/gtk.h>

#include "crush-app.h"

G_BEGIN_DECLS

#define CRUSH_TYPE_GRID crush_grid_get_type()

#define CRUSH_GRID(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  CRUSH_TYPE_GRID, CrushGrid))

#define CRUSH_GRID_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  CRUSH_TYPE_GRID, CrushGridClass))

#define CRUSH_IS_GRID(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  CRUSH_TYPE_GRID))

#define CRUSH_IS_GRID_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  CRUSH_TYPE_GRID))

#define CRUSH_GRID_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  CRUSH_TYPE_GRID, CrushGridClass))

typedef struct _CrushGrid CrushGrid;
typedef struct _CrushGridClass CrushGridClass;
typedef struct _CrushGridPrivate CrushGridPrivate;

struct _CrushGrid
{
  GtkGrid parent;

  CrushGridPrivate *priv;
};

struct _CrushGridClass
{
  GtkGridClass parent_class;
};

GType crush_grid_get_type (void) G_GNUC_CONST;

CrushGrid *crush_grid_new ();

// Recalculate button images. e.g. after a swap.
void crush_grid_update_buttons(CrushGrid*);

// Clear any current selection.
void crush_grid_deselect(CrushGrid*);

int8_t crush_grid_selected_row(CrushGrid*);
int8_t crush_grid_selected_column(CrushGrid*);

G_END_DECLS

#endif /* __CRUSH_GRID_H__ */
