#ifndef __CRUSH_TYPES_H__
#define __CRUSH_TYPES_H__

#if __cplusplus
  #include <cstdint>
#else
  #include <stdint.h>
#endif

typedef int8_t CandyInt;

typedef enum {
  Regular = 0
  // Later we may add Striped, etc.
} CandyType;

typedef enum {
  // Empty is a non-color, used in the game marking logic.
  Empty = -1,

  Blue = 0,
  Green = 1,
  Orange = 2,
  Purple = 3,
  Red = 4,
  Yellow = 5,
  ColorCount
} CandyColor;

typedef struct board_candy_st {
  CandyColor color;
  CandyType type;
} BoardCandy;

typedef enum {
  Left = 0,
  Right = 1,
  Up = 2,
  Down = 3
} SwapDirection;

typedef void (*UpdateUiCallback)();

#endif
