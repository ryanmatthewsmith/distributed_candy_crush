// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <functional>

extern "C" {
  #include "jansson.h"
}

#include "ClientSocket.h"
#include "SolverClient.h"
#include "CrushManager.h"
#include "BoardCopies.h"

using namespace std;
using namespace std::placeholders;

#define TEAM_NAME "<marquee>CHUBS</marquee>"

SolverClient::~SolverClient() {
  delete this->clientSocket;
}

void SolverClient::Start(std::string host, int port) {
  this->clientSocket = new hw5_net::ClientSocket(host, port);
  this->connectionStatus = SolverClientConnectionStatus::Handshaking;

  const char* helloPayload =
   "{\"action\" : \"hello\", \"teamname\":\"" TEAM_NAME "\"}";
  this->clientSocket->WrappedWrite(helloPayload, strlen(helloPayload));

  while (this->modelRead()) {
    // (do nothing.)
  }
}

void SolverClient::addBoard(shared_ptr<BoardCopies> board) {
  this->movesEvaluated++;

  unique_lock<mutex> lock(this->bestCopyMutex);

  if (!this->bestCopy || board->score > this->bestCopy->score) {
    this->bestCopy = board;
  }

  lock.unlock();

  this->boardQueue->put(board);
}

void SolverClient::workFunc() {
  // This is the entry function for all of our worker threads. Read a board
  // from the queue and explore the moves, which ends up pushing each valid
  // sub-board back onto the queue.

  while (true) {
    shared_ptr<BoardCopies> boardCopy = this->boardQueue->get();

    if (boardCopy == NULL) {
      break;
    }

    this->exploreMoves(boardCopy);
  }

  cerr << "get thread terminating" << endl;
}

void SolverClient::exploreMoves(std::shared_ptr<BoardCopies> board) {
  processCopy(
    board.get(),
    std::bind(&SolverClient::addBoard, this, _1),
    this->running
  );
}

json_t* SolverClient::stringToJson(string toJson){
  const char *cstr = toJson.c_str();
  return json_loads(cstr, JSON_DECODE_ANY, NULL);
}

const char* SolverClient::actionToString(json_t * act){
  json_t* actionStr = json_object_get(act, "action");
  assert(actionStr != NULL);
  return json_string_value(actionStr);
}

void SolverClient::sendBestMove() {
  assert(this->bestCopy != NULL);
  auto boardState = this->bestCopy->initState;

  printf(
   "Sending best move: move count = %d, score = %d, row = %d, col = %d, "
   "dir = %d, moves evaluated = %u. (depth = %d.)\n",
   boardState->moveCount, boardState->score,
   this->bestCopy->row, this->bestCopy->col, this->bestCopy->direction,
   (unsigned) this->movesEvaluated, this->bestCopy->depth);

  json_t* json = json_object();
  json_object_set_new(json, "action", json_string("mymove"));
  json_object_set_new(json, "teamname", json_string(TEAM_NAME));
  json_object_set_new(json, "gameinstance", boardState->toJson());

  json_t* moveJson = json_object();
  json_object_set_new(moveJson, "row",
   json_integer(this->bestCopy->row));
  json_object_set_new(moveJson, "column",
   json_integer(this->bestCopy->col));
  json_object_set_new(moveJson, "direction",
   json_integer(this->bestCopy->direction));

  json_object_set_new(json, "move", moveJson);
  json_object_set_new(json, "movesevaluated",
   json_integer(this->movesEvaluated));

  char* payload = json_dumps(json, 0);
  json_decref(json);

  this->clientSocket->WrappedWrite(payload, strlen(payload));
  free(payload);
}

bool SolverClient::modelRead(){
  bool returnVal = false;
  if(!this->clientSocket->isConnected()) {
    this->connectionStatus = SolverClientConnectionStatus::Inactive;
    cerr << "Disconnected." << endl;
    return false;
  }
  char buffer[1024];
  int bracesCount = 0;
  string bufInString;
  this->clientSocket->WrappedRead(buffer, 1);
  if (buffer[0] == '{'){
    bufInString+=buffer[0];
    bracesCount++;
  } else{
    cerr << "invalid json: " << bufInString << endl;
    return false;
  }
  uint16_t i = 0;
  while(bracesCount != 0){
    int readct = this->clientSocket->WrappedRead(buffer, 1023);

    assert(readct != -1);

    for (; i < readct ; i++){
      bufInString += buffer[i];
      if (buffer[i] == '{') bracesCount++;
      if (buffer[i] == '}') bracesCount--;
      if (bracesCount == 0)break;
      }
      i = 0;
  }

  json_t *json = stringToJson(bufInString);
  const char* action = actionToString(json);

  if (strcmp(action, "requestmove") == 0) {
    if (this->connectionStatus == SolverClientConnectionStatus::Active) {

      this->running = false;

      this->sendBestMove();
      shared_ptr<BoardCopies> c = this->bestCopy;
      this->bestCopy = NULL;

      this->boardQueue->dump();

      this->running = true;
      // Reseed the queue by synchronously evaluating this new best board.
      c->currentState = NULL;
      c->depth = 0;
      this->exploreMoves(c);

      returnVal = true;
    } else {
      cerr << "action \"requestmove\" invalid for "
        "current connection state." << endl;
    }
  }
  else if (strcmp(action, "helloack") == 0) {
    if (this->connectionStatus == SolverClientConnectionStatus::Handshaking) {
      this->connectionStatus = SolverClientConnectionStatus::Active;

      CrushState* state = new CrushState(json_object_get(json, "gameinstance"));

      shared_ptr<BoardCopies> copy = std::make_shared<BoardCopies>();
      copy->initState.reset(state);

      this->boardQueue =
       new SynchronizedQueue<shared_ptr<BoardCopies>, BoardCopiesComparer>();
      this->running = true;
      this->exploreMoves(copy);

      this->workers =
       new std::array<std::unique_ptr<std::thread>, SolverClient::kNumThreads>();

      for (int i = 0; i < SolverClient::kNumThreads; i++) {
        this->workers->at(i).reset(new std::thread(
         std::bind(&SolverClient::workFunc, this)));
      }

      returnVal = true;
    } else {
      cerr << "action \"helloack\" invalid for "
        "current connection state." << endl;
    }
  }
  else if(strcmp(action, "bye") == 0) {
    if (this->connectionStatus == SolverClientConnectionStatus::Active) {
      // Exit loop.
      returnVal = false;
      this->connectionStatus = SolverClientConnectionStatus::Inactive;
    } else {
      cerr << "action \"bye\" invalid for "
        "current connection state." << endl;
    }
  }
  else {
    cerr << "Invalid message action \"" << action << "\"" << endl;
  }

  json_decref(json);
  return returnVal;
}
