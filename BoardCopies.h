// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __BOARDCOPIES_H__
#define __BOARDCOPIES_H__

#include <memory>
#include <iostream>

#include "CrushState.h"

/**
 * BoardCopies, for managing board copies.
 */
class BoardCopies {
public:
   BoardCopies();

   void init(int16_t row, int16_t col, int8_t direction);
   BoardCopies* boardCopy();

public:
   std::shared_ptr<CrushState> initState;
   std::shared_ptr<CrushState> currentState;
   int16_t row;
   int16_t col;
   int8_t direction;
   int16_t score;
   int8_t depth;

private:
   friend std::ostream& operator<<(std::ostream& os, const BoardCopies& c);
};

#endif //__BOARDCOPIES_H__
