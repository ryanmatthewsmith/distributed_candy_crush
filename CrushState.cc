#include <stdexcept>
#include <iostream>

#include "CrushState.h"

#include "crush-types.h"

static void deserializeCandyInt(json_t* json, ArrValue destination) {
  int value = json_integer_value(json);
  *((CandyInt*)destination) = value;
}

static void serializeCandyInt(json_t** jsonDestination, ArrValue arrayValue) {
  *jsonDestination = json_integer(*(CandyInt*)arrayValue);
}

static void deserializeBoardCandy(json_t* json, ArrValue destination) {
  BoardCandy* candy = (BoardCandy*) destination;

  candy->color = (CandyColor)
   json_integer_value(json_object_get(json, "color"));
  candy->type = (CandyType)
   json_integer_value(json_object_get(json, "type"));
}

static void serializeBoardCandy(json_t** jsonDestination, ArrValue arrayValue) {
  BoardCandy* candy = (BoardCandy*) arrayValue;
  json_t* json = json_object();
  json_object_set_new(json, "color", json_integer(candy->color));
  json_object_set_new(json, "type", json_integer(candy->type));
  *jsonDestination = json;
}

CrushState::CrushState(json_t* json) {
  // Read various properties from json.

  // Keep gamedef around, because we include it verbatim in the final
  // serialization.

  this->gameDefJson = json_object_get(json, "gamedef");
  json_incref(this->gameDefJson);

  int colors = json_integer_value(
   json_object_get(this->gameDefJson, "colors"));

  if (colors > ColorCount) {
    char errorMsg[100];
    sprintf(errorMsg,
     "Specified color count %d more than supported %d.",
     colors, ColorCount);
    throw std::runtime_error(errorMsg);
  }

  json_t* boardState = json_object_get(this->gameDefJson, "boardstate");

  int rows = json_integer_value(json_object_get(boardState, "rows"));
  int cols = json_integer_value(json_object_get(boardState, "columns"));
  this->rows = rows;
  this->cols = cols;

  json_t* extensionState =
   json_object_get(this->gameDefJson, "extensioncolor");

  this->extBoard = Array2D_newFromJson(
   extensionState, sizeof(CandyInt), deserializeCandyInt);

  this->extensionOffsets.resize(cols);

  json_t* gameState = json_object_get(json, "gamestate");

  if (gameState == NULL) {
    // New game. Create empty board array.
    this->newGame = true;
    this->moveCount = 0;
    this->score = 0;
    this->boardArray = Array2D_new(rows, cols, sizeof(BoardCandy));
    this->boardState = Array2D_newFromJson(boardState, sizeof(CandyInt),
     deserializeCandyInt);
  } else {
    // Saved game. Override some objects from there.
    this->newGame = false;

    this->moveCount = json_integer_value(
     json_object_get(gameState, "movesmade"));
    this->score = json_integer_value(
     json_object_get(gameState, "currentscore"));

    // Populate extension offsets.
    json_t* extensionOffsetsJson =
     json_object_get(gameState, "extensionoffset");

    size_t extIndex;
    json_t* extValue;

    json_array_foreach(extensionOffsetsJson, extIndex, extValue) {
      this->extensionOffsets.at(extIndex) = json_integer_value(extValue);
    }

    json_t* boardCandies = json_object_get(gameState, "boardcandies");

    this->boardArray = Array2D_newFromJson(
     boardCandies, sizeof(BoardCandy), deserializeBoardCandy);

    json_t* boardState = json_object_get(gameState, "boardstate");

    this->boardState = Array2D_newFromJson(boardState, sizeof(CandyInt),
     deserializeCandyInt);
  }
}

CrushState::CrushState(const CrushState& other) :
  moveCount(other.moveCount),
  score(other.score),
  newGame(other.newGame),
  gameDefJson(json_deep_copy(other.gameDefJson)),
  boardArray(Array2D_copy(other.boardArray)),
  boardState(Array2D_copy(other.boardState)),
  extBoard(Array2D_copy(other.extBoard)),
  extensionOffsets(other.extensionOffsets)
  // ^^^ we can tune these back a bit if performance is bad.
{ }

CrushState::~CrushState() {
  json_decref(this->gameDefJson);
  Array2D_free(this->boardArray);
  Array2D_free(this->boardState);
  Array2D_free(this->extBoard);
}

json_t* CrushState::toJson() const {
  json_t* jsonRoot = json_object();
  json_object_set(jsonRoot, "gamedef", this->gameDefJson);

  json_t* gameState = json_object();
  json_object_set_new(gameState, "boardcandies",
   Array2D_arrayToJson(this->boardArray, serializeBoardCandy));
  json_object_set_new(gameState, "boardstate",
   Array2D_arrayToJson(this->boardState, serializeCandyInt));
  json_object_set_new(gameState, "movesmade", json_integer(this->moveCount));
  json_object_set_new(gameState, "currentscore", json_integer(this->score));

  json_t* extensionOffsets = json_array();

  for (int offset : this->extensionOffsets) {
    json_array_append_new(extensionOffsets, json_integer(offset));
  }

  json_object_set_new(gameState, "extensionoffset", extensionOffsets);
  json_object_set_new(jsonRoot, "gamestate", gameState);
  return jsonRoot;
}
