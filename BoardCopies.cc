// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include "BoardCopies.h"

BoardCopies::BoardCopies()
  : initState(NULL), currentState(NULL), row(-1), col(-1), depth(0) {}

void BoardCopies::init(int16_t row, int16_t col, int8_t direction){
  this->row = row;
  this->col = col;
  this->direction = direction;
  this->score = 0;
}

BoardCopies* BoardCopies::boardCopy(){
  BoardCopies* newB = new BoardCopies();
  newB->init(this->row, this->col, this->direction);
  newB->initState = this->initState;
  newB->currentState.reset(new CrushState(*this->currentState));
  newB->score = this->score;
  newB->depth = this->depth;
  return newB;
}

std::ostream& operator<<(std::ostream& os, const BoardCopies& c) {
   return os << "Score " << c.score << " at depth " << c.depth
    << " from board " << c.initState.get() << " to board " << c.currentState.get();
}
