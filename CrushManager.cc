// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <queue>
#include <cassert>
#include <vector>
#include <iostream>

#include "CrushManager.h"
#include "BoardCopies.h"
#include "crush-types.h"

void processCopy(BoardCopies *board,
    std::function<void(std::shared_ptr<BoardCopies>)> processFnc,
    std::atomic_bool& running){

  bool firstMove = board->currentState == NULL;

  if (firstMove) {
    assert(board->initState != NULL);
    board->currentState = board->initState;
  }

  CrushManager topMgr;
  topMgr.setState(board->currentState.get());
  std::vector<std::vector<int>> validMoves = topMgr.possibleMoves();

  for(std::vector<int> moves : validMoves){

    std::shared_ptr<CrushState> currState(new CrushState(*board->currentState));

    CrushManager mgr;
    mgr.setState(currState.get());
    mgr.swap(moves.at(0), moves.at(1), (SwapDirection)moves.at(2));

    std::shared_ptr<BoardCopies> newBoard(board->boardCopy());
    newBoard->currentState = currState;
    newBoard->depth++;
    newBoard->score = currState->getScore();

    if (firstMove) {
      newBoard->initState = currState;
      // Only care to track these values on the root move.
      newBoard->row = moves.at(0);
      newBoard->col = moves.at(1);
      newBoard->direction = moves.at(2);
    }
    if(newBoard->score > 0 && moves.at(3) > 0) newBoard->score += moves.at(3);

    if (!running) {
      break;
    }

    processFnc(newBoard);
  }
}

CrushManager::CrushManager() : state(NULL), clientCommunicator(NULL) { }

CrushManager::~CrushManager() {}

void CrushManager::init() {
  assert(this->state != NULL);

  if (this->state->isNewGame()) {
    this->initBoard();
    this->gravity();
  }

  this->matchLoop();
}

void CrushManager::setState(CrushState* state) {
  delete this->state;
  this->state = state;
}

std::vector<std::vector<int>> CrushManager::possibleMoves(){
  std::vector<std::vector<int>> validMoves; //int array [row, col, direction, bonus]
  int c = Array2D_getCols(this->state->boardArray);
  int r = Array2D_getRows(this->state->boardArray);
  int j;

  for(int i = 0; i < r; i++){
    if(i%2 == 1) j = 1;
    else j = 0;
    for (; j < c; j+=2){
      for(int k = 0; k < 4; k++){
        if(validMove(i,j,(SwapDirection)k)){
          std::vector<int> validLoopMoves;
          validLoopMoves.push_back(i);
          validLoopMoves.push_back(j);
          validLoopMoves.push_back(k);
          int bonus=0;
          //extra bonus bottom corners are the hardest to fire
          //because gravity is down and few available moves
          if (this->state->score < r*c*6/10){
            if (j == 0 || j==c-1 || i == r-1 || i == 0) bonus++;
            if (i==0){
              //bonus ++;
              if ((j==0) && (k == 2 || k == 1))bonus++;
              if ((j==1 && k == 0) || (j==c-2 && k==1))bonus+=10;
              if ((j==c-1) && (k ==0 || k ==2))bonus++;
            }
            if (i==1 && j==0 && k==3)bonus+=10;
            if (i==1 && j==c-1 && k==3)bonus+=10;
            //top corner into bonus
            if (i==r-2&&k==2){
              if (j==0 || j==c-1)bonus+=10;
            }
            if(i==r-1){
               if ((j==1 && k==0)||(j==c-2&&k==1))bonus+=10;
            }
          }
          validLoopMoves.push_back(bonus);

          validMoves.push_back(validLoopMoves);
          validLoopMoves.clear();
          }  
      } 
    }
  }
  return validMoves;
}

bool CrushManager::swap(int8_t sourceRow, int8_t sourceCol, SwapDirection direction) {

  int8_t destRow = sourceRow;
  int8_t destCol = sourceCol;

  switch (direction) {
    case Down:
    destRow--;
    break;

    case Up:
    destRow++;
    break;

    case Left:
    destCol--;
    break;

    case Right:
    destCol++;
    break;

  default:
    assert(0);
  }

  bool swapped = true;

  if (Array2D_swap(this->state->boardArray, sourceRow, sourceCol, destRow, destCol)) {
    this->state->moveCount++;

    if (!this->matchLoop()) {
      // Reset moveCount, undo swap if no matches found.
      this->state->moveCount--;
      Array2D_swap(this->state->boardArray, sourceRow, sourceCol, destRow, destCol);
      swapped = false;
    } //if matchLoop
  } //if swap


  return swapped;
}

bool CrushManager::validMove(int8_t sourceRow, int8_t sourceCol, SwapDirection direction){
   bool matched;
   int8_t destRow = sourceRow;
   int8_t destCol = sourceCol;

   switch (direction) {
      case Down:
      destRow--;
      break;

      case Up:
      destRow++;
      break;

      case Left:
      destCol--;
      break;

      case Right:
      destCol++;
      break;

      default:
      assert(0);
   } 
   if (Array2D_swap(this->state->boardArray, sourceRow, sourceCol, destRow, destCol)) {
      bool V = matchCheckV();
      bool H  = matchCheckH();
      matched = (V || H);

      Array2D_swap(this->state->boardArray, sourceRow, sourceCol, destRow, destCol);
  
      if (matched) {
         return true;
      } else {
         return false;
      }
   }
   return false;
}

bool CrushManager::matchLoop(){
  const int MATCH_MAX = 1000;
  int loopCount = 0;
  bool matched;

  do {
    bool V4 = matchV(4);
    bool H4 = matchH(4);
    bool V3 = matchV(3);
    bool H3 = matchH(3);
    matched = (V4 || H4 || V3 || H3);

    if (matched) {
      gravity();
    } else if (loopCount == 0) {
      return false;
    }

    loopCount++;
  } while (loopCount < MATCH_MAX && matched);

  return true;
}

bool CrushManager::matchV(int len){
  bool found = false;
  int matches = 0;
  int currentMatch=-2;
  ArrValue temp;
  BoardCandy element;
  int c = Array2D_getCols(this->state->boardArray);
  int r = Array2D_getRows(this->state->boardArray);

  for (int i = 0; i<c; i++){
    for(int j = r-1; j>=0; j--){
      Array2D_get(this->state->boardArray,j,i,&temp);
      element = *(BoardCandy *)temp;
      if(element.color == Empty){
        currentMatch = -2;
        matches = 0;
      } else if(element.color == currentMatch){
        matches++;
        if (matches == len){
          found = true;
          for(int k = j+len-1; k>=j;k--){
            Array2D_get(this->state->boardArray,k,i,&temp);
            updateBoardState(k,i);
            element = *(BoardCandy *)temp;
            element.color = Empty;//-1;
            Array2D_set(this->state->boardArray,k,i,&element);
          } 
        currentMatch = -1;
        //*(int *)temp = -1;
        matches = 0;
        }
      }else{
        currentMatch = element.color;
        matches = 1;
      }
    }//for i
    matches=0;
    currentMatch=-2;
  }//for j
  return found;
}


bool CrushManager::matchH(int len){
  int matches = 0;
  int currentMatch=-2;
  bool found = false;
  ArrValue temp;
  BoardCandy element;
  int c = Array2D_getCols(this->state->boardArray);
  int r = Array2D_getRows(this->state->boardArray);

  for (int i = 0; i<r; i++){
    for(int j = 0; j<c; j++){
      Array2D_get(this->state->boardArray,i,j,&temp);
      element = *(BoardCandy *)temp;
      if(element.color == Empty){
        currentMatch = -2;
        matches = 0;
      } else if(element.color == currentMatch){
        matches++;
        if (matches == len){
          found = true;
          for(int k = (j-len+1); k<=j ;k++){
            Array2D_get(this->state->boardArray,i,k,&temp);
            updateBoardState(i,k);
            element = *(BoardCandy *)temp;
            element.color = Empty;//-1;
            Array2D_set(this->state->boardArray,i,k,&element);
          } 
        currentMatch = -1;
        //*(int *)temp = -1;
        matches = 0;
        }
      }else{
        currentMatch = element.color;
        matches = 1;
      }
    }//for i
    matches = 0;
    currentMatch = -2;
  }//for j
  return found;
}

bool CrushManager::matchCheckV(){
  int matches = 0;
  int currentMatch=-2;
  int c = Array2D_getCols(this->state->boardArray);
  int r = Array2D_getRows(this->state->boardArray);
  ArrValue temp;
  BoardCandy element;

  for (int i = 0; i<c; i++){
    for(int j = r-1; j>=0; j--){
      Array2D_get(this->state->boardArray,j,i,&temp);
      element = *(BoardCandy *)temp;
      if(element.color == Empty){
        currentMatch = -2;
        matches = 0;
      } else if(element.color == currentMatch){
        matches++;
        if (matches == 3){
          return true;
        }
      }else{
        currentMatch = element.color;
        matches = 1;
      }
    }//for i
    matches=0;
    currentMatch=-2;
  }//for j
  return false;
}


bool CrushManager::matchCheckH(){
  int matches = 0;
  int currentMatch=-2;
  int c = Array2D_getCols(this->state->boardArray);
  int r = Array2D_getRows(this->state->boardArray);
  ArrValue temp;
  BoardCandy element;

  for (int i = 0; i<r; i++){
    for(int j = 0; j<c; j++){
      Array2D_get(this->state->boardArray,i,j,&temp);
      element = *(BoardCandy *)temp;
      if(element.color == Empty){
        currentMatch = -2;
        matches = 0;
      } else if(element.color == currentMatch){
        matches++;
        if (matches == 3){
          return true;
        }
      }else{
        currentMatch = element.color;
        matches = 1;
      }
    }//for i
    matches = 0;
    currentMatch = -2;
  }//for j
  return false;
}

void CrushManager::updateBoardState(int row, int col){
   ArrValue jelly;
   Array2D_get(this->state->boardState,row,col,&jelly);
   CandyInt dec = *(CandyInt *)jelly;
     if(dec > 0){ 
       dec--;
       this->state->score++;
       jelly = &dec;
       Array2D_set(this->state->boardState,row,col,jelly);
     }
}

void CrushManager::getExtension(int num, int column, std::queue<int>&values){
   int startIndex = this->state->extensionOffsets.at(column);
   int height = Array2D_getRows(this->state->extBoard);
   for (int i = startIndex; i < (startIndex+num); i++){
     ArrValue temp;
     Array2D_get(this->state->extBoard,i%height,column,&temp);    
     values.push(*(CandyInt*)temp);
   }
   this->state->extensionOffsets.at(column) = ((startIndex+num)%height);
} 


void CrushManager::gravity(){

  using std::queue;

  int count = -1;
  int firstIndex=-1;  

  queue<int> myQ;

  int r = Array2D_getRows(this->state->boardArray);
  int c = Array2D_getCols(this->state->boardArray);

  ArrValue arrVal;
  BoardCandy element;
  for(int i=0;i<c;i++){
    count = -1;
    for(int j=0;j<r;j++){
      Array2D_get(this->state->boardArray, j, i, &arrVal);
      element = *(BoardCandy*)arrVal;
      if(count==-1 && element.color == Empty){
        firstIndex = j;
        count=1;    
      }else if(element.color == Empty){
        count++; 
      }else if(count>0 && element.color!=Empty){
      myQ.push(element.color);
      }    
    }//for j
    
    if(count > 0){
      while(myQ.size() > 0){
        element.color = (CandyColor)myQ.front();
        myQ.pop();
        Array2D_set(this->state->boardArray,firstIndex,i,&element);
        firstIndex++;
      } 
      getExtension(count, i, myQ);
      while(myQ.size() > 0){
        element.color = (CandyColor)myQ.front();
        myQ.pop();
        Array2D_set(this->state->boardArray,firstIndex,i,&element);
        firstIndex++;
      }
    }
    count = -1;
    firstIndex=-1;    
  }//for i
}

void CrushManager::initBoard() const {
  Iter_Array2D iter = Array2D_iterNew(this->state->boardArray);

  while (Array2D_iterHasNext(iter)) {
    ArrValue val = Array2D_iterNext(iter);
    BoardCandy* candy = (BoardCandy*) val;
    candy->color = Empty;
    candy->type = Regular;
  }

  Array2D_iterFree(iter);
}
