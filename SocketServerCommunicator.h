// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __SOCKETSERVERCOMMUNICATOR_H__
#define __SOCKETSERVERCOMMUNICATOR_H__

extern "C" {
  #include "jansson.h"
}

#include "ServerSocket.h"
#include "ClientSocket.h"

#include "Communicator.h"
#include "crush-types.h"

enum class ConnectionState {
  Down,
  Listening,
  Handshaking,
  Active
};

class SocketServerCommunicator : public ServerCommunicator {
public:
   SocketServerCommunicator() :
     state(NULL), serverSocket(NULL), clientSocket(NULL),
     connectionState(ConnectionState::Down) {}

   ~SocketServerCommunicator() {
      delete this->serverSocket;
      delete this->clientSocket;
      delete this->state;
   }

   virtual void Move(int8_t, int8_t, int);

   void SetCallback(UpdateUiCallback callback) {
      this->updateUiCallback = callback;
   }

   virtual CrushState* GetState() const {
      return this->state;
   }

   virtual void SetState(CrushState* state) {
      delete this->state;
      this->state = state;
   }

   void Listen();
   void AcceptClient();
   void SendBye();

private:
   void helloFunc();
   void updateFunc();
   json_t* stringToJson(string toJson);
   const char * actionToString(json_t * act);
   void viewRead();

   CrushState* state;
   hw5_net::ServerSocket* serverSocket;
   hw5_net::ClientSocket* clientSocket;

   ConnectionState connectionState;
   UpdateUiCallback updateUiCallback;
};

#endif
