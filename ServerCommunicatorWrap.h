// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

/*
 * This presents a C-callable wrapper to ServerCommunicator for use from the C
 * GTK+ interface code.
 */

#ifdef __cplusplus
extern "C" {
#endif
   #include <jansson.h>
   #include "array2D.h"
#ifdef __cplusplus
}
#endif

#include "crush-types.h"

typedef void CServerCommunicator;

#ifdef __cplusplus
extern "C" {
#endif

CServerCommunicator* CServerCommunicator_newFromJson(json_t*, UpdateUiCallback);
void CServerCommunicator_initServerWithJson(
      CServerCommunicator* comm,
      json_t* json, UpdateUiCallback callback);
void CServerCommunicator_delete(CServerCommunicator*);
void CServerCommunicator_start(CServerCommunicator*);
Array2D CServerCommunicator_getBoardArray(CServerCommunicator*);
Array2D CServerCommunicator_getBoardState(CServerCommunicator*);
json_t* CServerCommunicator_toJson(CServerCommunicator*);
int CServerCommunicator_getMoveCount(CServerCommunicator*);
int CServerCommunicator_getScore(CServerCommunicator*);
void CServerCommunicator_swap(CServerCommunicator*, int8_t, int8_t, int);

#ifdef __cplusplus
}
#endif
