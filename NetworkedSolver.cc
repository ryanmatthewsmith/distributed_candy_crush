// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <iostream>

extern "C" {
  #include "jansson.h"
}

#include "SolverClient.h"

using namespace std;

void usage(const char *exeName) {
  cout << "Usage: " << exeName << " host port" << endl;
  cout << "  Connect to host::port then send data and read replies" << endl;
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  int port;

  try {
    port = stoi(argv[2]);
  } catch (...) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  string host(argv[1]);
  SolverClient *client = new SolverClient();

  try {
    client->Start(host, port);
  } catch (string errString) {
    cerr << errString << endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
