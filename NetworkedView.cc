// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <iostream>
#include <string>
#include <cstdlib>
#include <sstream>
#include <cstring>
#include <cassert>

#include "ServerCommunicatorWrap.h"

extern "C" {
  #include "jansson.h"
  #include <gtk/gtk.h>
  #include "crush-app.h"

  CServerCommunicator* gServerCommunicator = NULL;
  int gUseNetwork = 1;
}

#include "ServerSocket.h"
#include "ClientSocket.h"
#include "SocketServerCommunicator.h"

using namespace std;

extern "C" void updateUi();

void usage() {
  cout << "Usage: newGame or filename(json game definition)" << endl;
}

int main(int argc, char **argv) {
   if (argc != 2) {
      usage();
      return EXIT_FAILURE;
   }

   json_error_t jsonError;
   json_t* jsonRoot = json_load_file(argv[1], 0, &jsonError);

   if (jsonRoot == NULL) {
     fprintf(stderr, "Error loading JSON: %s. (%s)\n",
      jsonError.text, jsonError.source);
     return EXIT_FAILURE;
   }

   SocketServerCommunicator serverCommunicator;

   if (gUseNetwork) {
      CrushState* initialState = new CrushState(jsonRoot);
      serverCommunicator.SetState(initialState);
      serverCommunicator.SetCallback(updateUi);
   } else {
     // jdjukasnf
   }

   gServerCommunicator = static_cast<CServerCommunicator*>(
       static_cast<ServerCommunicator*>(&serverCommunicator));

   serverCommunicator.Listen();

   while (TRUE) {
     serverCommunicator.AcceptClient();

     CrushApp* app = crush_app_new();
     int result = g_application_run(G_APPLICATION(app), 1, argv);
     assert(0 == result);
     g_object_unref(app);

     serverCommunicator.SendBye();
   }

   return EXIT_SUCCESS;
}
