// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>

#include "ClientSocket.h"

extern "C" {
  #include "jansson.h"
}

using namespace std;

#include "SocketClientCommunicator.h"

SocketClientCommunicator::~SocketClientCommunicator() {
   delete this->clientSocket;
}

void SocketClientCommunicator::Update(CrushState* state) {
   json_t* json = json_object();
   json_object_set_new(json, "action", json_string("update"));
   json_object_set_new(json, "gameinstance", state->toJson());

   char* payload = json_dumps(json, 0);
   json_decref(json);

   this->clientSocket->WrappedWrite(payload, strlen(payload));
   free(payload);
}

void SocketClientCommunicator::Start(std::string host, int port) {
    this->clientSocket = new hw5_net::ClientSocket(host, port);
    this->connectionStatus = ClientConnectionStatus::Handshaking;

    string hello = "{ \"action\" : \"hello\" }";
    this->clientSocket->WrappedWrite(hello.c_str(), hello.length());

    while(modelRead()){
    	;
    }
}

void SocketClientCommunicator::helloAckFunc(){
   //take the game instance update
   //if change reserialize new instance to json, make string, WrappedWrite
   //else gameinstance to string and WrappedWrite
}

void SocketClientCommunicator::moveFunc(json_t* swap){
   // {"action": "move", "row": 6, "column": 7, "direction": 0}
   json_t *row = json_object_get(swap, "row");
   json_t *col = json_object_get(swap, "column");
   json_t *direction = json_object_get(swap, "direction");
   int8_t rowI = (int8_t)(json_integer_value(row));
   int8_t colI = (int8_t)(json_integer_value(col));
   int dirI = (int)(json_integer_value(direction));

   this->crushManager->swap(rowI, colI, (SwapDirection)dirI);
   this->Update(this->crushManager->getState());
}

json_t* SocketClientCommunicator::stringToJson(string toJson){
  const char *cstr = toJson.c_str();
  return json_loads(cstr, JSON_DECODE_ANY, NULL);
}

const char * SocketClientCommunicator::actionToString(json_t * act){
  json_t * actionStr = json_object_get(act, "action");
  if (actionStr == NULL)printf("uh oh, no action in json");
  const char *actionCstr = json_string_value(actionStr);  
  return actionCstr; 
}
 
bool SocketClientCommunicator::modelRead(){
    bool returnVal = false;
    if(!(this->clientSocket->isConnected())) {
      this->connectionStatus = ClientConnectionStatus::Inactive;
      return false;
    }
    char buffer[1024];    
    int bracesCount = 0;
    string bufInString;
    this->clientSocket->WrappedRead(buffer, 1);
    if (buffer[0] == '{'){
      bufInString+=buffer[0];
      bracesCount++;
    } else{
      cout << "invalid json" << endl;
      return false;
    }
    uint16_t i = 0;
    while(bracesCount != 0){
      this->clientSocket->WrappedRead(buffer, 1023);
      for (; i< 1023 ; i++){
        bufInString += buffer[i];
        if (buffer[i] == '{') bracesCount++;
        if (buffer[i] == '}') bracesCount--; 
        if (bracesCount == 0)break;
        }
        i=0;
    }
    
    json_t *json = stringToJson(bufInString);
    const char* action = actionToString(json); 

    if(strcmp(action,"helloack") == 0){
      if (this->connectionStatus == ClientConnectionStatus::Handshaking) {
        this->crushManager->setState(
         new CrushState(json_object_get(json, "gameinstance")));

        this->crushManager->init();
        this->Update(this->crushManager->getState());
        returnVal = true;

        this->connectionStatus = ClientConnectionStatus::Active;
      } else {
        cerr << "action \"helloack\" invalid for "
          "current connection state." << endl;
      }
    }
    else if(strcmp(action,"move") == 0){
      if (this->connectionStatus == ClientConnectionStatus::Active) {
        moveFunc(json);
        returnVal = true;
      } else {
        cerr << "action \"move\" invalid for "
          "current connection state." << endl;
      }
    }
    else if(strcmp(action,"bye") == 0){
      if (this->connectionStatus == ClientConnectionStatus::Active) {
        // Exit loop.
        returnVal = false;
        this->connectionStatus = ClientConnectionStatus::Inactive;
      } else {
        cerr << "action \"bye\" invalid for "
          "current connection state." << endl;
      }
    }
    else {
      cerr << "Invalid message action \"" << action << "\"" << endl;
    }

    json_decref(json);
    return returnVal;
}
