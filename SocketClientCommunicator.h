// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __SOCKETCLIENTCOMMUNICATOR_H__
#define __SOCKETCLIENTCOMMUNICATOR_H__

#include <string>

extern "C" {
  #include "jansson.h"
}

#include "ClientSocket.h"
#include "Communicator.h"
#include "CrushManager.h"
#include "CrushState.h"
#include "crush-types.h"

enum class ClientConnectionStatus {
  Inactive,
  Handshaking,
  Active
};

class SocketClientCommunicator : public ClientCommunicator {
public:
   SocketClientCommunicator() :
     crushManager(NULL),
     connectionStatus(ClientConnectionStatus::Inactive) { }

   virtual ~SocketClientCommunicator();

   virtual void Update(CrushState* state);

   void Start(std::string host, int port);

   void SetCrushManager(CrushManager* mgr) {
      this->crushManager = mgr;
   }

private:
   void helloAckFunc();

   void moveFunc(json_t* swap);
   json_t* stringToJson(string toJson);

   const char * actionToString(json_t * act);

   bool modelRead();

   CrushManager* crushManager;
   hw5_net::ClientSocket* clientSocket;

   ClientConnectionStatus connectionStatus;
};

#endif
