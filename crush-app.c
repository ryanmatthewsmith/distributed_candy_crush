// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <jansson.h>

#include "array2D.h"

#include "crush-app.h"
#include "crush-app-window.h"

#include "ServerCommunicatorWrap.h"

#define APP_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CRUSH_TYPE_APP, CrushAppPrivate))

extern CServerCommunicator* gServerCommunicator;
Array2D gBoardArray = NULL;
Array2D gBoardState = NULL;

struct _CrushAppPrivate {
  char dummy; // avoid compilation warning
};

G_DEFINE_TYPE_WITH_PRIVATE(CrushApp, crush_app, GTK_TYPE_APPLICATION)

static void crush_app_shutdown(GApplication* application) {
  G_APPLICATION_CLASS(crush_app_parent_class)->shutdown(application);
}

static void crush_app_startup(GApplication* application) {
  G_APPLICATION_CLASS(crush_app_parent_class)->startup(application);
}

static void crush_app_init(CrushApp *app) {
  // Init private data.
  app->priv = APP_PRIVATE(app);
}

static void crush_app_activate(GApplication* application) {
  CrushApp* app = CRUSH_APP(application);
  // Success. Show the main window.

  CrushAppWindow* window = crush_app_window_new(app, NULL);
  gtk_window_present(GTK_WINDOW(window));
  crush_app_window_open(window);
}

static void crush_app_finalize(GObject* object) {
  G_OBJECT_CLASS(crush_app_parent_class)->finalize(object);

  // Serialize JSON.
  json_t* jsonRoot = CServerCommunicator_toJson(gServerCommunicator);

  if (json_dump_file(jsonRoot, "test.out", JSON_COMPACT) != 0) {
    fprintf(stderr, "failed to write JSON.\n");
  }

  json_decref(jsonRoot);
}

static void crush_app_class_init(CrushAppClass* class) {
  GApplicationClass* appClass = G_APPLICATION_CLASS(class);
  GObjectClass* objectClass = G_OBJECT_CLASS(class);

  g_set_application_name("Candy Crush");

  appClass->startup = crush_app_startup;
  appClass->shutdown = crush_app_shutdown;
  appClass->activate = crush_app_activate;

  objectClass->finalize = crush_app_finalize;
}

CrushApp* crush_app_new() {
  CrushApp* crushApp = g_object_new(crush_app_get_type(),
   "application-id", "edu.uw.crush",
   "flags", G_APPLICATION_HANDLES_OPEN,
   NULL);
  return crushApp;
}
