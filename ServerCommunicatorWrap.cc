// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <stdexcept>
#include <iostream>

#include "SocketServerCommunicator.h"
#include "LocalCommunicator.h"
#include "crush-types.h"

extern "C" {

#include "ServerCommunicatorWrap.h"

CServerCommunicator* CServerCommunicator_newFromJson(
      json_t* json, UpdateUiCallback callback) {
   try {
      // Local version. We create everything and return the server half.
      CrushManager* manager = new CrushManager();
      CrushState* initialState = new CrushState(json);

      LocalCommunicator* communicator = new LocalCommunicator();
      communicator->SetState(initialState);
      communicator->SetCrushManager(manager);
      communicator->SetCallback(callback);

      manager->setState(initialState);
      manager->setClientCommunicator(communicator);
      manager->init();

      return static_cast<CServerCommunicator*>((ServerCommunicator*)communicator);
   } catch (const std::runtime_error& error) {
      std::cerr << error.what() << std::endl;
   }

   return NULL;
}

void CServerCommunicator_initServerWithJson(
      CServerCommunicator* comm,
      json_t* json, UpdateUiCallback callback) {
   try {
      SocketServerCommunicator* communicator = static_cast<SocketServerCommunicator*>(comm);
      CrushState* initialState = new CrushState(json);
      communicator->SetState(initialState);
      communicator->SetCallback(callback);
   } catch (const std::runtime_error& error) {
      std::cerr << error.what() << std::endl;
   }
}

void CServerCommunicator_delete(CServerCommunicator* mgr) {
   ServerCommunicator* c = static_cast<ServerCommunicator*>(mgr);
   delete c;
}

void CServerCommunicator_start(CServerCommunicator* mgr) {
   ServerCommunicator* c = static_cast<ServerCommunicator*>(mgr);
   c->Start();
}

Array2D CServerCommunicator_getBoardArray(CServerCommunicator* mgr) {
   ServerCommunicator* c = static_cast<ServerCommunicator*> (mgr);
   return c->GetState()->getBoardArray();
}

Array2D CServerCommunicator_getBoardState(CServerCommunicator* mgr) {
   ServerCommunicator* c = static_cast<ServerCommunicator*>(mgr);
   return c->GetState()->getBoardState();
}

json_t* CServerCommunicator_toJson(CServerCommunicator* mgr) {
   ServerCommunicator* c = static_cast<ServerCommunicator*>(mgr);
   return c->GetState()->toJson();
}

int CServerCommunicator_getMoveCount(CServerCommunicator* mgr) {
   ServerCommunicator* c = static_cast<ServerCommunicator*>(mgr);
   return c->GetState()->getMoveCount();
}

int CServerCommunicator_getScore(CServerCommunicator* mgr) {
   ServerCommunicator* c = static_cast<ServerCommunicator*>(mgr);
   return c->GetState()->getScore();
}

void CServerCommunicator_swap(CServerCommunicator* mgr,
 int8_t sourceRow, int8_t sourceCol, int direction) {
   ServerCommunicator* c = static_cast<ServerCommunicator*>(mgr);
   c->Move(sourceRow, sourceCol, direction);
}

} // extern C
