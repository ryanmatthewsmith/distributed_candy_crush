// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>
#include "ClientSocket.h"

extern "C" {
  #include "jansson.h"
}

#include "SocketClientCommunicator.h"
#include "CrushManager.h"

using namespace std;

void usage(const char *exeName) {
  cout << "Usage: " << exeName << " host port" << endl;
  cout << "  Connect to host::port then send data and read replies" << endl;
  exit(1);
}

int main(int argc, char *argv[]) {

  if ( argc != 3 ) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  int port;
  try {
    port = stoi(argv[2]);
  } catch (...) {
    usage(argv[0]);
  }

  string host(argv[1]);

  SocketClientCommunicator client;
  CrushManager manager;
  client.SetCrushManager(&manager);
  manager.setClientCommunicator(&client);

  try {
    client.Start(host, port);

  } catch(string errString) {
    cerr << errString << endl;
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
}

