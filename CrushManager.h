// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#ifndef __CRUSHMANAGER_H__
#define __CRUSHMANAGER_H__

#include <vector>
#include <queue>
#include <functional>
#include <atomic>
#include <memory>

extern "C" {
   #include "array2D.h"
}

#include "Communicator.h"
#include "CrushState.h"
#include "crush-types.h"
#include "BoardCopies.h"

void processCopy(BoardCopies*,
      std::function<void(std::shared_ptr<BoardCopies>)>,
      std::atomic_bool&);

/**
 * CrushManager.
 * Controls Candy Crush gameplay and logic.
 */
class CrushManager {
public:
   CrushManager();
   ~CrushManager();

   void init();

   int getMoveCount() const { return this->state->getMoveCount(); }
   int getScore() const { return this->state->getScore(); }
   Array2D getBoardArray() const { return this->state->getBoardArray(); }
   Array2D getBoardState() const { return this->state->getBoardState(); }

   CrushState* getState() { return this->state; }
   void setState(CrushState* state);

   void setClientCommunicator(ClientCommunicator* c) {
      this->clientCommunicator = c;
   }

   /**
    * Swaps the candy at the given row/col in the given direction.
    */
    bool swap(int8_t sourceRow, int8_t sourceCol, SwapDirection direction);

    /**
    *   generates list of available crush moves for current board
    *
    *   returns Vector of valid moves
    *
    */
    std::vector<std::vector<int>> possibleMoves(); 

private:

   /**
   *   checks for valid swap
   *
   *   returns true if the swap is valid
   */
   bool validMove(int8_t sourceRow, int8_t sourceCol, SwapDirection direction);

   /**
    *  handles template match and gravity loop
    *
    *  Returns:
    *    false if no template matches found
    *    true otherwise
    */
   bool matchLoop();

   /**
    *  Checks for vert, horizontal templates
    *
    *  Param:
    *    int len: length of desired template
    *  Returns:
    *    true if match found
    */
   bool matchV(int len);
   bool matchH(int len);
   bool matchCheckV();
   bool matchCheckH();

   /**
    *  Fills in holes left from template firings
    */
   void gravity();

   /*
    * updates score and boardstate for exploded elements
    *
    * Param:
    *   int row: row of boardState to update
    *   int col: column of boardState to update
    */
   void updateBoardState(int row, int col);

   /**
    *  mutates passed queue to add extension values
    *
    *  Parameters:
    *    int num, number of needed CandyInt from extBoard
    *    int col, column of extBoard
    *    queue<int> values, reference to queue we use to pass
    *               CandyInt back to boardArray
    */
   void getExtension(int num, int column, std::queue<int>& values);

   /**
    * Clear board array in event of a brand new game.
    */
   void initBoard() const;

private:
   CrushState* state;
   ClientCommunicator* clientCommunicator;
};

#endif
