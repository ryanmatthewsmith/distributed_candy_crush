// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

/* crush-app-window.h */

#ifndef __CRUSH_APP_WINDOW_H__
#define __CRUSH_APP_WINDOW_H__

#include <gtk/gtk.h>

#include "crush-app.h"

G_BEGIN_DECLS

#define CRUSH_TYPE_APP_WINDOW crush_app_window_get_type()

#define CRUSH_APP_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  CRUSH_TYPE_APP_WINDOW, CrushAppWindow))

#define CRUSH_APP_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  CRUSH_TYPE_APP_WINDOW, CrushAppWindowClass))

#define CRUSH_IS_APP_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  CRUSH_TYPE_APP_WINDOW))

#define CRUSH_IS_APP_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  CRUSH_TYPE_APP_WINDOW))

#define CRUSH_APP_WINDOW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  CRUSH_TYPE_APP_WINDOW, CrushAppWindowClass))

typedef struct _CrushAppWindow CrushAppWindow;
typedef struct _CrushAppWindowClass CrushAppWindowClass;
typedef struct _CrushAppWindowPrivate CrushAppWindowPrivate;

struct _CrushAppWindow
{
  GtkApplicationWindow parent;

  CrushAppWindowPrivate *priv;
};

struct _CrushAppWindowClass
{
  GtkApplicationWindowClass parent_class;
};

GType crush_app_window_get_type (void) G_GNUC_CONST;

CrushAppWindow *crush_app_window_new(CrushApp* app, json_t* json);

void crush_app_window_open(CrushAppWindow* window);

G_END_DECLS

#endif /* __CRUSH_APP_WINDOW_H__ */
