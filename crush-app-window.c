// David Grant (dagr), Ryan Matthew Smith (rsmith24)
// CSE333 17wi

#include <assert.h>

#include "array2D.h"

#include "crush-app-window.h"
#include "crush-grid.h"

#include "ServerCommunicatorWrap.h"

#define APP_WINDOW_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CRUSH_TYPE_APP_WINDOW, CrushAppWindowPrivate))

extern CServerCommunicator* gServerCommunicator;
extern int gUseNetwork;
extern Array2D gBoardArray;
extern Array2D gBoardState;

struct _CrushAppWindowPrivate
{
  GtkWidget* mainBox;
  GtkWidget* moveCountLabel;
  GtkWidget* scoreLabel;
  CrushGrid* crushGrid;
};

G_DEFINE_TYPE_WITH_PRIVATE (CrushAppWindow, crush_app_window, GTK_TYPE_APPLICATION_WINDOW)

static CrushAppWindow* gWindow = NULL;

static void
crush_app_window_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
crush_app_window_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
crush_app_window_dispose (GObject *object)
{
  G_OBJECT_CLASS (crush_app_window_parent_class)->dispose (object);
}

static void
crush_app_window_finalize (GObject *object)
{
  G_OBJECT_CLASS (crush_app_window_parent_class)->finalize (object);
}

static void update_labels() {
  CrushAppWindowPrivate* priv = crush_app_window_get_instance_private(gWindow);
  int moveCount = CServerCommunicator_getMoveCount(gServerCommunicator);
  int score = CServerCommunicator_getScore(gServerCommunicator);

  char labelBuf[20];

  sprintf(labelBuf, "Move count: %d", moveCount);
  gtk_label_set_text(GTK_LABEL(priv->moveCountLabel), labelBuf);

  sprintf(labelBuf, "Score: %d", score);
  gtk_label_set_text(GTK_LABEL(priv->scoreLabel), labelBuf);
}

static void swap(SwapDirection direction) {
  CrushAppWindowPrivate* priv = crush_app_window_get_instance_private(gWindow);

  int8_t row = crush_grid_selected_row(priv->crushGrid);
  int8_t col = crush_grid_selected_column(priv->crushGrid);

  crush_grid_deselect(priv->crushGrid);
  CServerCommunicator_swap(gServerCommunicator, row, col, direction);
}

void updateUi() {
  // Update everything. The underlying state might have changed entirely, so we
  // re-fetch anything we use to redraw the UI.

  CrushAppWindowPrivate* priv = crush_app_window_get_instance_private(gWindow);

  if (priv == NULL) {
    // Initial updates occur before priv exists. This is fine.
    return;
  }

  gBoardArray = CServerCommunicator_getBoardArray(gServerCommunicator);
  gBoardState = CServerCommunicator_getBoardState(gServerCommunicator);
  crush_grid_update_buttons(priv->crushGrid);
  update_labels();
}

static void up_clicked(GtkWidget* w) {
  swap(Up);
}
static void down_clicked(GtkWidget* w) {
  swap(Down);
}
static void left_clicked(GtkWidget* w) {
  swap(Left);
}
static void right_clicked(GtkWidget* w) {
  swap(Right);
}

static void crush_app_window_class_init (CrushAppWindowClass* klass)
{
  GObjectClass* object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = crush_app_window_get_property;
  object_class->set_property = crush_app_window_set_property;
  object_class->dispose = crush_app_window_dispose;
  object_class->finalize = crush_app_window_finalize;

  GtkWidgetClass* widgetClass = GTK_WIDGET_CLASS(klass);

  gtk_widget_class_set_template_from_resource(widgetClass,
   "/edu/uw/crush/crush-app-window.ui");

  gtk_widget_class_bind_template_child_private(widgetClass,
   CrushAppWindow, mainBox);
  gtk_widget_class_bind_template_child_private(widgetClass,
   CrushAppWindow, moveCountLabel);
  gtk_widget_class_bind_template_child_private(widgetClass,
   CrushAppWindow, scoreLabel);

  gtk_widget_class_bind_template_callback(widgetClass, up_clicked);
  gtk_widget_class_bind_template_callback(widgetClass, down_clicked);
  gtk_widget_class_bind_template_callback(widgetClass, left_clicked);
  gtk_widget_class_bind_template_callback(widgetClass, right_clicked);
}

static void crush_app_window_init(CrushAppWindow* self)
{
  gtk_widget_init_template(GTK_WIDGET(self));
  self->priv = APP_WINDOW_PRIVATE(self);
}

CrushAppWindow* crush_app_window_new(CrushApp* app, json_t* json)
{
  gBoardArray = CServerCommunicator_getBoardArray(gServerCommunicator);
  gBoardState = CServerCommunicator_getBoardState(gServerCommunicator);

  CrushAppWindow* window = g_object_new(CRUSH_TYPE_APP_WINDOW, "application", app, NULL);
  gWindow = window;
  return window;
}

void crush_app_window_open(CrushAppWindow* window) {
  CrushAppWindowPrivate* priv = crush_app_window_get_instance_private(window);

  priv->crushGrid = crush_grid_new(NULL);

  gtk_box_pack_start(GTK_BOX(priv->mainBox), GTK_WIDGET(priv->crushGrid), TRUE, FALSE, 0);
  update_labels();

  gtk_widget_show(GTK_WIDGET(priv->crushGrid));
  gtk_widget_show(GTK_WIDGET(priv->mainBox));
}
